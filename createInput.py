import sys
from random import choice
from string import ascii_lowercase

if(len(sys.argv) != 1):
    n = int(sys.argv[1])
    if(n <= 5000):
        s = ''
        for i in range(n):
            s += str(choice(ascii_lowercase))
        print (s)
    else:
        print ('Please enter the length of the string to be less than 5000')
else:
    print ("Usage: python createInput.py <String length N>")
