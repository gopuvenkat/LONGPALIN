#include <stdio.h>

int main() {
    // INPUT
    int n;
    scanf("%d",&n);
    char s[n];
    scanf("%s",s);

    int si = -1;								// Starting index of the longest palindrome
    int len = 0;								// Length of the longest palindrome
    int templen;

    for(int i = 0; i<n;i++) {							// Assuming that the longest palindrome is of odd length
        templen = 1;
        int iter = 1;
        while(i-iter > -1 && i+iter < n && s[i-iter] == s[i+iter]) {
            templen += 2;
            iter++;
        }
        if(templen > len) {                             //Updating the length of the palindrome if found longer
            len = templen;                              //than the previous found palindrome
            si = i-iter+1;
        }
    }

    for(int i = 0; i<n-1;i++) {							// Assuming that the longest palindrome is of even length
        templen = 0;
        int iter = 0;
        while(i-iter > -1 && i+iter+1 < n && s[i-iter] == s[i+iter+1]) {
            templen += 2;
            iter++;
        }
        if(templen > len) {                             //Updating the length of the palindrome if found longer
            len = templen;                              //than the previous found palindrome
            si = i-iter+1;
        }
    }

    printf("%d\n",len);								// Print the length of the palindrome
    for(int i = si; i<si+len; i++) {
        printf("%c",s[i]);							// Print the corresponding substring
    }
    printf("\n");
}
